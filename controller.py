from contact import Contact
from model import ContactModel
from exception import ContactAlreadyExistsException, ContactNotFoundException
from view import ContactView


class ContactController:
    def __init__(self, view: ContactView, model: ContactModel):
        self.view = view
        self.model = model

    def add_contact(self, name, email):
        try:
            contact = self.model.create(Contact(name, email))
            self.view.display_contact_created(contact)
        except ContactAlreadyExistsException:
            self.view.display_email_already_exists(email)

    def list_contacts(self):
        contacts = self.model.get_all()
        self.view.display_contacts_list(contacts)

    def get_contact_details(self, value=None, field='email'):
        try:
            if field == 'email':
                contact = self.model.find_by_email(value)
            elif field == 'id':
                contact = self.model.find_by_id(int(value))
            self.view.display_contact_details(contact)
        except ContactNotFoundException:
            self.view.display_contact_not_found(
                f'field: {field} | value: {value}')
        except Exception as e:
            self.view.display_generic_error(str(e))

    def delete_contact(self, id):
        try:
            id = int(id)
        except ValueError as e:
            self.view.display_generic_error(str(e))
            return

        try:
            self.model.delete(id)
            self.view.display_contact_deleted(id)
        except ContactNotFoundException:
            self.view.display_contact_not_found(f'id: {id}')

    def update_contact(self, id, name=None, email=None):
        try:
            id = int(id)
        except ValueError as e:
            self.view.display_generic_error(str(e))
            return

        try:
            self.model.update(id, Contact(name, email))
            self.view.display_contact_updated()
        except ContactAlreadyExistsException:
            self.view.display_email_already_exists(email)
        except ContactNotFoundException:
            self.view.display_contact_not_found(f'id: {id}')
        except Exception as e:
            self.view.display_generic_error(str(e))
