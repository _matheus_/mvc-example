class ContactView:
    @staticmethod
    def display_contacts_list(contacts):
        print('\n')
        print('--- CONTACT LIST ---')

        if contacts:
            for contact in contacts:
                print(f'[{contact.id}] {contact.name} ({contact.email})')
        else:
            print('** empty list **')

        print('\n')

    @staticmethod
    def display_contact_details(contact):
        print('\n')
        print('--- CONTACT DETAILS ---')
        print(f'ID: {contact.id}\nNAME: {contact.name}\nEMAIL: {contact.email}')
        print('-----------------------')
        print('\n')

    @staticmethod
    def display_contact_created(contact):
        message = f'The contact {contact.name} ({contact.email}) was created with id [{contact.id}]'
        ContactView._display_message_box('CREATED', message)

    @staticmethod
    def display_contact_deleted(id):
        message = f'The contact [{id}] was removed!'
        ContactView._display_message_box('DELETED', message)

    @staticmethod
    def display_contact_not_found(search_term):
        message = f'OPS!! There is no match with the term "{search_term}"'
        ContactView._display_message_box('NOT FOUND', message)

    @staticmethod
    def display_email_already_exists(email):
        message = f'OPS !! The email address "{email}" already exists'
        ContactView._display_message_box('ALREADY EXISTS', message)

    @staticmethod
    def display_generic_error(message=None):
        message = message if message is not None else 'Failed to complete this operation'
        ContactView._display_message_box('ERROR', message)

    @staticmethod
    def display_contact_updated():
        message = 'Contat updated successfully!'
        ContactView._display_message_box('UPDATED', message)

    @staticmethod
    def _display_message_box(title, message):
        message_len = len(message)
        print(f'\n*** {title} ***')
        print('='*message_len)
        print(message)
        print('='*message_len)
        print('\n')
