import os
from controller import ContactController
from model import ContactModel
from view import ContactView

_controller = ContactController(ContactView(), ContactModel())

menu_options = [
    (0, 'Add'),
    (1, 'List all'),
    (2, 'Show details'),
    (3, 'Remove'),
    (4, 'Update'),
    (-1, 'Exit')
]


while(True):
    print('================')
    print('Select an option')
    print('================')

    for option, label in menu_options:
        print(f'[{option}] {label}')

    try:
        try:
            option_selected = int(input('> Select an option: '))
            if option_selected >= len(menu_options):
                print('** Select a valid option **')
        except ValueError:
            continue

        if option_selected == -1:
            break
        elif option_selected == 0:
            os.system('clear')
            name = input('> Name: ')
            email = input('> Email: ')
            _controller.add_contact(name, email)
        elif option_selected == 1:
            os.system('clear')
            _controller.list_contacts()
        elif option_selected == 2:
            os.system('clear')
            field = input('> Find by (email|id): ')
            value = input('> Value: ')
            _controller.get_contact_details(field=field, value=value)
        elif option_selected == 3:
            os.system('clear')
            id = input('> Id: ')
            _controller.delete_contact(id)
        elif option_selected == 4:
            os.system('clear')
            id = input('> Id: ')
            name = input('> Name: ')
            email = input('> Email: ')
            _controller.update_contact(id, name, email)

    except KeyboardInterrupt:
        print('\nBye!!\n')
        break
    except Exception as e:
        print(e)
