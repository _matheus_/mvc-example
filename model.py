from backend import *


class ContactModel:
    def create(self, contact):
        return add_contact(contact)

    def get_all(self):
        return get_contacts()

    def find_by_id(self, id):
        return read_contact(id)

    def find_by_email(self, email):
        return read_contact(email)

    def delete(self, id):
        delete_contact(id)

    def update(self, id, contact):
        email_already_exists = False
        try:
            contact_with_email = self.find_by_email(contact.email)
            if contact_with_email.id != id:
                email_already_exists = True
        except:
            pass

        if email_already_exists:
            raise ContactAlreadyExistsException()

        return update_contact(id, contact)
