from exception import ContactAlreadyExistsException, ContactNotFoundException

contacts = []


def add_contact(contact):
    id = len(contacts) + 1
    contact.id = id
    find = list(filter(lambda c: c.email == contact.email, contacts))
    if find:
        raise ContactAlreadyExistsException()
    contacts.append(contact)
    return contact


def get_contacts():
    return contacts


def read_contact(emailOrId):
    result = list(filter(
        lambda c: c.id == emailOrId or c.email == emailOrId, contacts))
    if not result:
        raise ContactNotFoundException()
    return result[0]


def update_contact(id, new_contact):
    contact = read_contact(id)

    if new_contact.name:
        contact.name = new_contact.name
    if new_contact.email:
        contact.email = new_contact.email


def delete_contact(id):
    try:
        index = id - 1
        del contacts[index]
    except KeyError:
        raise ContactNotFoundException()
