class Contact:
    def __init__(self, name, email) -> None:
        self.id = None
        self.name = name
        self.email = email

    def __repr__(self) -> str:
        return f'Contact(id={self.id}, name={self.name}, email={self.email})'

    def __str__(self) -> str:
        return f'Contact(id={self.id}, name={self.name}, email={self.email})'
